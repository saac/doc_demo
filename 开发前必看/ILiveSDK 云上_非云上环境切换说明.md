# ILiveSDK 云上_非云上环境切换说明

## 一. 背景
云上版本是腾讯视频云为优化海外节点准备的一套新的音视频环境。音视频sdk也支持云上和非云上环境在进音视频房间的时候自由选择。

> 注意：一个房间里的人，如果一部分用云上版本，一部分用自研版本，会导致这两部分人音视频不互通。


## 二. 云上版本SDK下载更新
* PC：下载替换最新版本的 [ILiveSDK](http://dldir1.qq.com/hudongzhibo/git/iLiveSDK_PC_Suixinbo/iLiveSDK/iLiveSDK_1.9.0.2.zip)

* Android：更新 iLIveSDK gradle 依赖为

```
compile 'com.tencent.ilivesdk:ilivesdk:1.9.1'
```

* iOS：下载替换最新版本的 [ILiveSDK](http://dldir1.qq.com/hudongzhibo/ILiveSDK/ILiveSDK_1.9.1.13841.zip) 和 [AVSDK](http://dldir1.qq.com/hudongzhibo/ILiveSDK/AVSDK_1.9.8.32.38749.zip) 

* web：加载2.4版本js

```js
<script src="https://sqimg.qq.com/expert_qq/webrtc/2.4/WebRTCAPI.min.js"></script>
```

## 三. 切换云上环境
**切换云上环境只需要两步：**
### 1. 切换环境
云上版本SDK默认即为云上环境。

但是，如果开发者手动设置了非云上环境，则需将手动设置非云上环境的代码去除，或者将其替换为以下手动设置云上环境的代码：
#### PC
在初始化 ILiveSDK 之前，添加以下代码，切换非云上环境

```c++
GetILive()->setChannelMode(E_ChannelIMRestAPI);
```

#### Android
在初始化 ILiveSDK 之前，添加以下代码，切换为云上环境

```java
ILiveSDK.getInstance().setChannelMode(CommonConstants.E_ChannelMode.E_ChannelIMRestAPI);
```

#### iOS
在初始化 ILiveSDK 之前，添加以下代码，切换为云上环境

```
[[ILiveSDK getInstance] setChannelMode:E_ChannelIMRestAPI withHost:@""];
```


#### Web
初始 WebRTCAPI 传 `useCloud:1`

```js
var RTC = new WebRTCAPI( {
    "userId": userId,
    "sdkAppId":  sdkappid,
    "accountType":  accountType,
    "userSig": userSig,
    "useCloud": 1
});
```

### 2. 创建房间/进入房间时增加 privateMapKey 参数
#### privateMapKey的派发

云上环境，需要进房的时候多指定privateMapKey(房间票据)这个一个参数，相当于进入指定房间(roomId)的钥匙。

生成方法参考生成[UserSig和privateMapKey计算源码下载（java | PHP | nodejs）](https://github.com/TencentVideoCloudMLVBDev/usersig_server_source) 。

#### 各端进房调用方法说明

#### PC
iliveSDK 进房间选项增加 privateMapKey 参数，此项从业务后台获取，进房间设置，代码如下：

```c++
ilive::iLiveRoomOption roomOption;
roomOption.privateMapKey = //业务后台生成的privateMapKey;
ilive::GetILive()->joinRoom(roomOption, oniLiveJoinRoomSuc, oniLiveJoinRoomErr, this);
```
#### Android
创建/加入房间时需要增加 privateMapKey 参数：

```java
// 1.通过业务后台接口获取privatemapkey
// 2.获取成功后则将privatemapkey通过进房参数设置生效，如下：
ILiveRoomOption livingroomOption = new ILiveRoomOption()
        ...
        .privateMapKey(//业务后台生成的privateMapKey)
        ...
```


#### iOS
创建/加入房间时需要在进房配置对象中设置 privateMapKey 参数： 

```objc
// 1.通过业务后台接口获取privatemapkey
// 2.获取成功后则将privatemapkey通过进房参数设置生效，如下：

ILiveRoomOption *option = [[ILiveRoomOption alloc] init];
option.avOption.privateMapKey = //业务后台生成的privateMapKey;
...

[[ILiveRoomManager getInstance] joinRoom:roomID option:option succ:^{
    // 进房成功
} failed:^(NSString *module, int errId, NSString *errMsg) {
    // 进房失败
}];

```


#### web
创建/加入房间时增加privateMapKey参数

```js
RTC.createRoom( {
    roomid : 123456,
    privateMapKey: "业务后台生成的privateMapKey",
    role : "user",
}, function(){
    console.debug( ' 进房房间成功 ')
} ,  function(data){
    console.debug( ' 进入房间失败 ' , data)
});
```

## 四. 切换非云上环境
**切换非云上环境也只需要两步：**
### 1. 切换环境
#### PC
在初始化 ILiveSDK 之前，添加以下代码，切换为非云上环境

```c++
GetILive()->setChannelMode(E_ChannelIMSDK);
```

#### Android
在初始化 ILiveSDK 之前，添加以下代码，切换为非云上环境

```java
ILiveSDK.getInstance().setChannelMode(CommonConstants.E_ChannelMode.E_ChannelIMSDK);
```

#### iOS
在初始化 ILiveSDK 之前，添加以下代码，切换为非云上环境

```
[[ILiveSDK getInstance] setChannelMode:E_ChannelIMSDK withHost:@""];
```


#### Web
初始 WebRTCAPI 传 `useCloud:0`

```js
var RTC = new WebRTCAPI( {
    "userId": userId,
    "sdkAppId":  sdkappid,
    "accountType":  accountType,
    "userSig": userSig,
    "useCloud": 0
});
```

### 2. 去除创建/进入房间时的 privateMapKey 参数





